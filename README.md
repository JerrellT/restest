RESTest
=======

This is a basic API CRUD functionality(ies), created using Symfony3 

Some notes:
 - Please install the dependency on composer.json and complete installation of PostgreSQL before proceeding
 - The database used to store the information are named "RESTest" by username "postgres" and password "test123". Please create the database beforehand and adjust the parameters accordingly.
 - The table on the database was created by migration. Please create the entity and run the migration to create ORM and database table.

The API consists of 5 routes on src/AppBundle/Controller/Api/RestaurantController
1. Add Restaurant
- Method : POST
- Content type : application/x-www-form-urlencoded
- Parameters : name [string, max 255 ; not nullable] and desc [string, max 65525]
- URL : /api/restaurant/add

2. Show Restaurant
- Method : GET
- Parameters : {id} [integer]
- URL : /api/restaurant/details/{id}

3. Show Restaurant List (no pagination yet)
- Method : GET
- URL : /api/restaurant/list

4. Update Restaurant
- Method : PUT
- Content type : application/x-www-form-urlencoded
- Parameters : {id} [integer], name [string, max 255 ; not nullable], desc [string, max 65525]
- URL : /api/restaurant/update/{id}

5. Delete Restaurant
- Method : DELETE
- Parameters : {id} [integer]
- URL : /api/restaurant/delete/{id}