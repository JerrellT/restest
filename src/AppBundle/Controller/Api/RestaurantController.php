<?php
/**
 * Created by PhpStorm.
 * User: msi
 * Date: 9/18/2018
 * Time: 10:38 PM
 */

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class RestaurantController extends Controller
{
    /**
     * @param Request $request
     * @Route("/api/restaurant/add",name="add_restaurant")
     * @Method("POST")
     * @return JsonResponse
     */
    public function addRestaurant(Request $request) {

        $restName = $request->get('name');
        $restDescription = $request->get('desc');

        $response=array(
            'code'=>0,
            'message'=>'Restaurant registered successfully'
        );

        if (empty($restName)){
            $response["code"]=1;
            $response["message"]="Restaurant Name not defined";
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }

        $newRest = new Restaurant();

        $newRest->setRestaurantName($restName);
        $newRest->setDescriptions($restDescription);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newRest);
        try {
            $em->flush();
            return new JsonResponse($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            $response["code"]=2;
            $response["message"]="Error saving to server with exception : \n".$ex->getMessage();
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @param $id
     * @Route("/api/restaurant/details/{id}",name="show_restaurant")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function showRestaurant($id)
    {
        $post=$this->getDoctrine()->getRepository('AppBundle:Restaurant')->find($id);

        $response=array(
            'code'=>0,
            'message'=>'Restaurant information fetched successfully'
        );

        if (empty($post)){
            $response["code"]=1;
            $response["message"]="Restaurant ID not found";
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $data=$this->get('jms_serializer')->serialize($post,'json');
        try {
            $response['result']=json_decode($data);
            return new JsonResponse($response, Response::HTTP_OK);
        }
        catch(\Exception $ex){
            $response["code"]=2;
            $response["message"]="Internal error with exception : ".$ex->getMessage();
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/api/restaurant/list",name="list_restaurants")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function listRestaurants()
    {
        $posts=$this->getDoctrine()->getRepository('AppBundle:Restaurant')->findAll();

        $response=array(
            'code'=>0,
            'message'=>'Restaurants information fetched successfully'
        );

        if (!count($posts)){
            $response["code"]=1;
            $response["message"]="No registered Restaurant";
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $data=$this->get('jms_serializer')->serialize($posts,'json');
        try {
            $response['result']=json_decode($data);
            return new JsonResponse($response, Response::HTTP_OK);
        }
        catch(\Exception $ex){
            $response["code"]=2;
            $response["message"]="Internal error with exception : ".$ex->getMessage();
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @Route("/api/restaurant/update/{id}",name="update_restaurant")
     * @Method({"PUT"})
     * @return JsonResponse
     */
    public function updateRestaurant(Request $request,$id)
    {
        $post=$this->getDoctrine()->getRepository('AppBundle:Restaurant')->find($id);

        $response=array(
            'code'=>0,
            'message'=>'Restaurant updated successfully'
        );

        if (empty($post))
        {
            $response["code"]=1;
            $response["message"]="Restaurant ID not found";
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $body=$request->getContent();

        $restName = $request->get('name') ? !empty($request->get('name')) : $post->getRestaurantName() ;
        $restDescription = $request->get('desc');

        $post->setRestaurantName($restName);
        $post->setDescriptions($restDescription);
        $em=$this->getDoctrine()->getManager();
        $em->persist($post);
        try {
            $em->flush();
            return new JsonResponse($response, Response::HTTP_OK);
        }
        catch(\Exception $ex){
            $response["code"]=2;
            $response["message"]="Internal error with exception : ".$ex->getMessage();
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $id
     * @Route("/api/restaurant/delete/{id}",name="delete_restaurant")
     * @Method({"DELETE"})
     * @return JsonResponse
     */
    public function deleteRestaurant($id)
    {
        $post=$this->getDoctrine()->getRepository('AppBundle:Restaurant')->find($id);

        $response=array(
            'code'=>0,
            'message'=>'Restaurant deleted successfully'
        );

        if (empty($post)) {
            $response["code"]=1;
            $response["message"]="Restaurant ID not found";
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $em=$this->getDoctrine()->getManager();
        $em->remove($post);
        try {
            $em->flush();
            return new JsonResponse($response, Response::HTTP_OK);
        }
        catch(\Exception $ex){
            $response["code"]=2;
            $response["message"]="Internal error with exception : ".$ex->getMessage();
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}